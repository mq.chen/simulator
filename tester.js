const SignificanceTester = require('./significanceTester');

const s = new SignificanceTester([
  // { name: 'A', data: [6, 22] },
  // { name: 'B', data: [13, 15] }
  { name: 'A', data: ['null', 'yes', 'yes', 'yes', 'no'] },
  { name: 'B', data: ['no', 'yes', 'no', 'no', 'no', 'no', 'no'] }
], {
  countValue: 'yes'
});
console.log(s.dataDesc);

s.runTests();

s.on('complete', (result) => {
  console.log(result);
});
