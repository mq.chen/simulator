const simpleStats = require('simple-statistics');

// https://simplestatistics.org/docs/#permutationtest
// https://en.wikipedia.org/wiki/Resampling_(statistics)#Permutation_tests

module.exports = (groups, dataDesc, options) => {
  const out = {
    testName: 'Permutation-test',
    testResult: null,
    testRelevance: 0,
    testRelevanceReason: null,
    testLimitations: 'Permutation test assumes that the observations are exchangeable.'
  };

  // Reasons for test to be irrelevant
  if (dataDesc.groupsCount !== 2) {
    out.testRelevanceReason = 'Test can only compare two groups.';
    return out;
  }
  if (dataDesc.dataType === 'categorical') {
    out.testRelevanceReason = 'This test only works on numerical data.';
    return out;
  }
  if (options.compare !== 'mean') {
    out.testRelevanceReason = 'Permutation test only supports comparing means.';
    return out;
  }

  // Test is relevant
  if (dataDesc.dataType === 'numerical') {
    out.testRelevance = 1;
    out.testRelevanceReason = 'Data is numerical. Test compares difference of the means.';
  }

  if (out.testRelevance > 0) {
    out.testResult = {};
    out.testResult.leftPValue = simpleStats.permutationTest(groups[0].data, groups[1].data, 'less');
    out.testResult.rightPValue = simpleStats.permutationTest(groups[0].data, groups[1].data, 'greater');
    out.testResult.twoTailedPValue = simpleStats.permutationTest(groups[0].data, groups[1].data, 'two_side');
    out.testResult.oneTailedPValue = Math.min(
      out.testResult.leftPValue, out.testResult.rightPValue
    );
  }

  return out;
};
