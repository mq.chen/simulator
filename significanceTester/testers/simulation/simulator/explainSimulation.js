
module.exports = (simsSummary, actualDiff, groups) => {
  if (!(simsSummary instanceof Map) || simsSummary.size === 0) return '';
  const diffs = Array.from(simsSummary.keys()).sort((a, b) => a - b);
  const totalDiffs = Array.from(simsSummary.values()).reduce((a, b) => a + b);

  let belowActualDiffCount = 0;
  let equalActualDiffCount = 0;
  let aboveActualDiffCount = 0;

  diffs.forEach((diff) => {
    const simsCount = simsSummary.get(diff);
    if (diff < actualDiff.diff) belowActualDiffCount += simsCount;
    else if (diff === actualDiff.diff) equalActualDiffCount += simsCount;
    else aboveActualDiffCount += simsCount;
  });

  // Percentage of simulations compared to actual
  const pctBelowActual = (belowActualDiffCount / totalDiffs) * 100;
  const pctEqualActual = (equalActualDiffCount / totalDiffs) * 100;
  const pctAboveActual = (aboveActualDiffCount / totalDiffs) * 100;

  // Hypothesis prediction comparison:
  // Based on your results/input data, it seems your hypothesis predicted
  // Group A would have a [higher] [occurrence of item 'X'] than Group B ([+ 25%]).
  // [4%] of the simulations produced this result or [higher].
  // [96%] of the results produced [lower].
  // Hence, there is a [4%] possibility that your results are random and a [96%] probability
  // that your hypothesis is validated.

  let occuranceOrValue = '';
  if (actualDiff.compare === 'mean' || actualDiff.compare === 'median') {
    occuranceOrValue = `${actualDiff.compare} value`;
  } else if (actualDiff.compare === 'count' || actualDiff.compare === 'count-pct') {
    occuranceOrValue = `occurrence of '${actualDiff.countValue}'`;
  }

  let h = 'Based on your results/input data, it seems your hypothesis predicted that '
    + `Group '${groups[0].name}' would have a ${actualDiff.diff < 0 ? 'higher' : 'lower'} `
    + `${occuranceOrValue} than Group '${groups[1].name}'.\n`;

  if (actualDiff.diff < 0) { // higher
    h += `The simulation shows a ${(pctEqualActual + pctBelowActual).toFixed(1)} % `
      + `probability that your results are random and a ${pctAboveActual.toFixed(1)} % `
      + 'probability that your hypothesis is valid.';
  } else {
    h += `The simulation shows a ${(pctEqualActual + pctAboveActual).toFixed(1)} % `
      + `probability that your results are random and a ${pctBelowActual.toFixed(1)} % `
      + 'probability that your hypothesis is valid.';
  }

  return h;
};
