module.exports = (simResults) => {
  const simCountByResult = new Map();

  Array.from(simResults).forEach((diffObj) => {
    if (!simCountByResult.has(diffObj.diff)) {
      simCountByResult.set(diffObj.diff, 0);
    }
    simCountByResult.set(diffObj.diff, simCountByResult.get(diffObj.diff) + 1);
  });

  return simCountByResult;
};
