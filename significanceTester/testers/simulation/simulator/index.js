const EventEmitter = require('events');
const diffGroups = require('./diffGroups');
const runSimulation = require('./runSimulation');
const summarizeSims = require('./summarizeSims');
// const explainSimulation = require('./explainSimulation');

class Simulator extends EventEmitter {
  constructor(groups, dataDesc, inputOptions) {
    super();
    this.options = {
      compare: 'guess',
      countValue: null, // null = first item
      simulations: null,
      meanRoundDecimals: 1,
      countPctRoundDecimals: 1,
      skipDuplicateSims: false,
      ...inputOptions
    };
    this.groups = groups;
    this.dataDesc = dataDesc;
  }

  run() {
    if (this.options.simulations === null) {
      this.options.simulations = Math.min(this.dataDesc.sampleSize * 1000, 10 ** 6);
    }
    // compare actual data
    this.actualDiff = diffGroups(
      this.groups[0].data, this.groups[1].data,
      {
        ...this.options,
        uniqueValues: this.dataDesc.uniqueValues,
        dataPrimitive: this.dataDesc.dataPrimitive,
        dataType: this.dataDesc.dataType
      }
    );

    this.simulationResults = runSimulation(
      this.groups[0].data,
      this.groups[1].data,
      this.actualDiff,
      this.options.simulations,
      this.options.skipDuplicateSims,
      (currentCount, totalSims) => this.emit('progress', currentCount, totalSims)
    );
    this.simulationSummary = summarizeSims(this.simulationResults);

    this.emit('complete', {
      summary: this.simulationSummary,
      results: this.simulationResults,
      actualDiff: this.actualDiff,
      groups: this.groups,
      options: this.options,
      // testExplaination: explainSimulation(this.simulationSummary, this.actualDiff, this.groups),
      ...this.calcPValue()
    });
  }

  calcPValue() {
    const simsSummary = this.simulationSummary;
    const actualDiff = this.actualDiff;

    const diffs = Array.from(simsSummary.keys()).sort((a, b) => a - b);
    const totalDiffs = Array.from(simsSummary.values()).reduce((a, b) => a + b);

    let belowActualDiffCount = 0;
    let equalActualDiffCount = 0;
    let aboveActualDiffCount = 0;

    diffs.forEach((diff) => {
      const simsCount = simsSummary.get(diff);
      if (diff < actualDiff.diff) belowActualDiffCount += simsCount;
      else if (diff === actualDiff.diff) equalActualDiffCount += simsCount;
      else aboveActualDiffCount += simsCount;
    });

    // Percentage of simulations compared to actual
    const pctBelowActual = (belowActualDiffCount / totalDiffs);
    const pctEqualActual = (equalActualDiffCount / totalDiffs);
    const pctAboveActual = (aboveActualDiffCount / totalDiffs);

    const leftPValue = pctBelowActual + pctEqualActual;
    const rightPValue = pctAboveActual + pctEqualActual;
    const oneTailedPValue = Math.min(leftPValue, rightPValue);
    const twoTailedPValue = Math.min(oneTailedPValue * 2, 1);

    return {
      leftPValue,
      rightPValue,
      oneTailedPValue,
      twoTailedPValue
    };
  }
}

module.exports = Simulator;
