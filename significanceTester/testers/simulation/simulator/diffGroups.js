const simpleStats = require('simple-statistics');

function diffMedians(groupA, groupB) {
  const medianA = simpleStats.median(groupA);
  const medianB = simpleStats.median(groupB);
  return {
    values: [medianA, medianB],
    diff: medianB - medianA
  };
}

function diffMeans(groupA, groupB, options) {
  const meanA = simpleStats.mean(groupA);
  const meanB = simpleStats.mean(groupB);
  return {
    values: [meanA, meanB],
    diff: Math.round((meanB - meanA) * 10 ** options.meanRoundDecimals)
      / 10 ** options.meanRoundDecimals
  };
}

function diffCounts(groupA, groupB, options) {
  const countValue = options.countValue || options.uniqueValues.sort()[0];
  const countA = groupA.filter((e) => e === countValue).length;
  const countB = groupB.filter((e) => e === countValue).length;
  return {
    values: [countA, countB],
    diff: countB - countA,
    countValue
  };
}

function diffCountsPct(groupA, groupB, options) {
  const out = diffCounts(groupA, groupB, options);
  out.values = [out.values[0] / groupA.length, out.values[1] / groupB.length];
  out.diff = Math.round((out.values[1] - out.values[0]) * 10 ** (options.countPctRoundDecimals + 2))
    / 10 ** (options.countPctRoundDecimals + 2);
  return out;
}

module.exports = (groupA, groupB, inputOptions) => {
  const options = {
    compare: null,
    meanRoundDecimals: 1,
    countPctRoundDecimals: 1,
    ...inputOptions
  };

  // Comparing

  // get diffs
  let out = options;
  if (options.compare === 'median') {
    out = {
      ...out, ...diffMedians(groupA, groupB, options)
    };
  } else if (options.compare === 'mean') {
    out = {
      ...out, ...diffMeans(groupA, groupB, options)
    };
  } else if (options.compare === 'count') {
    out = {
      ...out, ...diffCounts(groupA, groupB, options)
    };
  } else if (options.compare === 'count-pct') {
    out = {
      ...out, ...diffCountsPct(groupA, groupB, options)
    };
  } else {
    throw new Error(`Unsupported compare method: ${options.compare}`);
  }

  return out;
};
