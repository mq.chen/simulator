const diffGroups = require('./diffGroups');

function shuffleArray(inputArray) {
  const array = inputArray.splice(0);

  let currentIndex = array.length;
  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    const randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    const temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

// Returns two groups of same size as A and B but items shuffled
function shuffleGroups(groupA, groupB) {
  const items = shuffleArray([...groupA, ...groupB]);
  return [
    items.slice(0, groupA.length).sort(),
    items.slice(groupA.length).sort()
  ];
}

module.exports = (groupA, groupB, diffObj, simsToRun, skipDuplicateSims, progressCallback) => {
  const progressCb = typeof progressCallback === 'function' ? progressCallback : () => {};
  const sims = new Map();

  for (let i = 0; i < simsToRun; i += 1) {
    progressCb.call(null, i + 1, simsToRun);
    const newGroup = shuffleGroups(groupA, groupB);
    const simSignature = skipDuplicateSims ? `${newGroup[0].join(',')}-${newGroup[1].join(',')}` : i;

    if (!sims.has(simSignature)) {
      sims.set(simSignature, diffGroups(newGroup[0], newGroup[1], diffObj));
    }
  }
  // return: simulation signature (sorted and hashed)
  // return: diff result
  return Array.from(sims.values());
};
