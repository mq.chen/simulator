const Simulator = require('./simulator');
const makeCalcDesc = require('./makeCalcDesc');

module.exports = async (groups, dataDesc, inputOptions) => new Promise((resolve) => {
  const out = {
    testName: 'Simulation',
    testResult: null,
    testRelevance: 0,
    testRelevanceReason: null, // is overriden by tests
    testLimitations: 'The test is imprecise if number of simulations is low, and it assumes that the groups are independent of each other.'
  };

  // Tests to see if simulation is relevant
  if (dataDesc.groupsCount !== 2) {
    out.testRelevanceReason = `Simulation only supports comparing 2 groups. Received ${dataDesc.groupsCount} groups instead.`;
    resolve(out);
    return;
  }
  if (dataDesc.sampleSize < 8) {
    out.testRelevanceReason = `Sample size is not large enough for meaningful simulation. Expects sample size ≥ 8, instead recevied ${dataDesc.sampleSize}.`;
    resolve(out);
    return;
  }

  // Run simulation
  const sim = new Simulator(groups, dataDesc, inputOptions);
  sim.on('complete', (result) => {
    out.testRelevance = 0.9; // Never 1 as this method is imprecise
    out.testRelevanceReason = `Dataset is large enough to create random groups for testing randomness. Ran ${result.options.simulations} simulations.`;
    if (result.actualDiff.compare === 'count' || result.actualDiff.compare === 'count-pct') {
      out.testRelevanceReason += ` Compared count of '${result.actualDiff.countValue}' in the groups.`;
    } else {
      out.testRelevanceReason += ` Compared the ${result.actualDiff.compare}s of the groups.`;
    }
    out.testResult = {
      options: result.options,
      calcDesc: { type: 'text', content: makeCalcDesc(result.summary, result.actualDiff) },
      leftPValue: result.leftPValue,
      rightPValue: result.rightPValue,
      oneTailedPValue: result.oneTailedPValue,
      twoTailedPValue: result.twoTailedPValue
    };
    resolve(out);
  });
  sim.run();
});
