const tTest = require('ttest');

// https://www.npmjs.com/package/ttest
// https://simplestatistics.org/docs/#ttesttwosample
// https://trendingsideways.com/the-p-value-formula-testing-your-hypothesis
// https://www.socscistatistics.com/pvalues/tdistribution.aspx

module.exports = (groups, dataDesc, options) => {
  const out = {
    testName: 'T-test',
    testResult: null,
    testRelevance: 0,
    testRelevanceReason: null,
    testLimitations: 'Assumes the groups follow similar distributions and that they are significanly smaller than the size of thee population.'
  };

  // Reasons the test is irrelevant:
  if (dataDesc.groupsCount !== 2) {
    out.testRelevanceReason = 'Test can only compare two groups.';
    return out;
  }
  if (dataDesc.dataType !== 'numerical') {
    out.testRelevanceReason = 'T-test only works with numerical data.';
    return out;
  }
  if (groups[0].data.length !== groups[1].data.length) {
    out.testRelevanceReason = 'T-test requires both groups samples to be equal.';
    return out;
  }
  if (groups[0].data.length < 5 || groups[1].data.length < 5) {
    out.testRelevanceReason = 'T-test is unsafe when a group\'s sample size is below 5.';
    return out;
  }
  if (options.compare !== 'mean') {
    out.testRelevanceReason = 'T-test is only relevant when comparing the means.';
    return out;
  }

  // Test relevance if test is relevant
  if (dataDesc.dataType === 'numerical') {
    out.testRelevance = 1;
    out.testRelevanceReason = 'Data is numerical. Test compares difference of the means.';
  }

  if (out.testRelevance > 0) {
    out.testResult = {};
    out.testResult.leftPValue = tTest(groups[0].data, groups[1].data, { alternative: 'less' }).pValue();
    out.testResult.rightPValue = tTest(groups[0].data, groups[1].data, { alternative: 'greater' }).pValue();
    out.testResult.twoTailedPValue = tTest(groups[0].data, groups[1].data, { alternative: 'not equal' }).pValue();
    out.testResult.oneTailedPValue = Math.min(
      out.testResult.leftPValue, out.testResult.rightPValue
    );

    const t = tTest(groups[0].data, groups[1].data);
    out.testResult.calcDesc = {
      type: 'text',
      content: `T-statistic: ${t.testValue()}\nDegrees of freedom: ${t.freedom()}`
    };
  }

  return out;
};
