
// https://www.socscistatistics.com/tests/ztest/default2.aspx
// https://mathcracker.com/z-test-for-two-proportions#results
// https://trendingsideways.com/the-p-value-formula-testing-your-hypothesis
// https://www.npmjs.com/package/ztable

module.exports = (groups, dataDesc, inputOptions) => {
  const out = {
    testName: 'Z score test',
    testResult: null,
    testRelevance: 0,
    testRelevanceReason: null,
    testLimitations: 'The test assumes that the data follows normal distribution.'
  };

  return out;
};
