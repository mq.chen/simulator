const fishersExactTest = require('fishers-exact-test');
const makeCalcDesc = require('./makeCalcDesc');

module.exports = (groups, dataDesc, inputOptions) => {
  const countValue = inputOptions.countValue;

  const out = {
    testName: 'Fisher\'s exact test',
    testResult: null,
    testRelevance: 0,
    testRelevanceReason: null,
    testLimitations: 'The test assumes that the groups are independent of each other.'
  };

  // Only relevant if data supplied is 2x2 of positive ints or categorical/strings
  let relevance = 0;
  let relevanceReason = 'Data is neither 2x2 matrix of positive integers or 2 groups of categorical data';

  // If data is 2x2
  if (dataDesc.dataPrimitive === 'int' && !dataDesc.hasNegativeValues
  && dataDesc.groupsCount === 2 && groups[0].data.length === 2 && groups[1].data.length === 2) {
    // If positive ints of 2x2 then relevance is good
    if ([...groups[0].data, ...groups[1].data].reduce((a, b) => a + b) >= 10000) {
      relevance = 0;
      relevanceReason = 'Sum of the numbers in the 2x2 matrix exceeds 10000';
    } else {
      relevance = 1;
      relevanceReason = '2x2 matrix of positive integers';
    }
  }

  let matrix = [...groups[0].data, ...groups[1].data];

  // If data is categorical then create matrix by counting occurences
  if (dataDesc.dataType === 'categorical' && dataDesc.groupsCount === 2) {
    const a = groups[0].data.filter((e) => e === countValue).length;
    const b = groups[0].data.length - a;
    const c = groups[1].data.filter((e) => e === countValue).length;
    const d = groups[1].data.length - c;

    matrix = [a, b, c, d];

    relevance = 1;
    relevanceReason = `Data is categorical. (Creating matrix by counting '${countValue}'.)`;
  }

  out.testRelevance = relevance;
  out.testRelevanceReason = relevanceReason;
  if (relevance > 0) {
    out.testResult = { ...fishersExactTest(...matrix) };
    out.testResult.calcDesc = { type: 'text', content: makeCalcDesc(groups, dataDesc, matrix, inputOptions) };
  }
  return out;
};
