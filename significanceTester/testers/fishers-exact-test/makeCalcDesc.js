const chalk = require('chalk');
const Table = require('terminal-table');

module.exports = (groups, dataDesc, matrix, options) => {
  // Create matrix for display
  const t = new Table({
    leftPadding: 1,
    rightPadding: 1,
    borderStyle: 2
  });
  t.attrRange({ row: [0, 99999], column: [1, 99999] }, { align: 'right' });
  if (dataDesc.dataType === 'categorical') {
    t.push(['', `'${options.countValue}'`, 'Other']);
  }
  t.push([groups[0].name, chalk.blue(matrix[0]), chalk.blue(matrix[1])]);
  t.push([groups[1].name, chalk.blue(matrix[2]), chalk.blue(matrix[3])]);
  return t.toString();
};
