const EventEmitter = require('events');
const simpleStats = require('simple-statistics');
const fishersExactTest = require('./testers/fishers-exact-test');
const simulation = require('./testers/simulation');
const permutationTest = require('./testers/permutation-test');
const tTest = require('./testers/t-test');

class SignificanceTester extends EventEmitter {
  constructor(groups, inputOptions) {
    super();

    this.options = {
      compare: 'guess',
      countValue: undefined, // First value (used if method is count)
      ...inputOptions
    };
    this.testers = {
      'fishers-exact-test': fishersExactTest,
      'permutation-test': permutationTest,
      't-test': tTest,
      simulation
    };
    this.dataDesc = this.descData(groups);
    this.options = { ...this.options, ...this.guessOptions() };
    this.groups = this.descGroups(groups);
  }

  guessOptions() {
    const out = {};
    if (['guess', null, undefined].includes(this.options.compare)) {
      out.compare = 'count-pct';
      if (this.dataDesc.dataType === 'numerical') {
        if (this.dataDesc.dataPrimitive === 'int') out.compare = 'median';
        if (this.dataDesc.dataPrimitive === 'float') out.compare = 'mean';
      }
    }
    if (['', null, undefined].includes(this.options.countValue)) {
      out.countValue = this.dataDesc.uniqueValues[0];
    }
    return out;
  }

  async runTests() {
    const out = [];
    const testerIds = Object.keys(this.testers);
    for (const testerId of testerIds) { /* eslint no-restricted-syntax: off */
      const tester = this.testers[testerId];
      if (typeof tester !== 'function') {
        throw new Error(`Expects testers to be functions. Tester '${testerId}' was of type ${typeof tester}.`);
      }
      const testResult = await tester /* eslint no-await-in-loop: off */
        .call(null, this.groups, this.dataDesc, this.options);

      // Validate tester output
      SignificanceTester.validateTesterTestResult(testerId, testResult);
      // testResult is validated and okay
      out.push({ testerId, ...testResult });
    }
    out.sort((a, b) => b.testRelevance - a.testRelevance);
    this.emit('complete', out);
  }

  static validateTesterTestResult(testerId, testResult) {
    if (!['testName', 'testResult', 'testRelevance', 'testRelevanceReason', 'testLimitations']
      .map((key) => Object.prototype.hasOwnProperty.call(testResult, key)).every((e) => e)) {
      throw new Error(`Tester '${testerId}' returned unexpected data. Expects object: { testName, testResult, testRelevance, testRelevanceReason, testLimitations }`);
    }
    if (!(testResult.testRelevance >= 0 && testResult.testRelevance <= 1)) {
      throw new Error(`Tester '${testerId}' returned testRelevance score (${testResult.testRelevance}) outside range of 0 to 1.`);
    }
    if (testResult.testRelevance === 0 && testResult.testResult !== null) {
      throw new Error(`Tester '${testerId}' should return null as testResult when testRelevance is 0. Instead returned '${JSON.stringify(testResult.testResult)}'.`);
    }
    if (testResult.testRelevance > 0
      && (typeof testResult.testResult !== 'object' || !Object.prototype.hasOwnProperty.call(testResult.testResult, 'oneTailedPValue'))) {
      throw new Error(`Tester '${testerId}' should return testResult as an object with at least the property: oneTailedPValue. Instead tester returned: ${JSON.stringify(testResult)}`);
    }
    if (testResult.testResult && Object.prototype.hasOwnProperty.call(testResult.testResult, 'calcDesc')
      && !(typeof testResult.testResult.calcDesc === 'object'
      && typeof testResult.testResult.calcDesc.type === 'string'
      && typeof testResult.testResult.calcDesc.content === 'string')) {
      throw new Error(`Tester '${testerId}' returned testResult.calcDesc but it did not match expected type/format: testResult.calcDesc = { type: 'text', content: '...' }.`);
    }
    return true;
  }

  descGroups(groups) {
    SignificanceTester.validateGroupsData(groups);

    for (const group of groups) {
      if (this.dataDesc.dataType === 'numerical') {
        group.mean = simpleStats.mean(group.data);
        group.median = simpleStats.median(group.data);
        group.standardDeviation = simpleStats.standardDeviation(group.data);
        group.variance = simpleStats.variance(group.data);
      }
      if (this.dataDesc.dataType === 'categorical') {
        group.countValueNum = group.data.filter((e) => e === this.options.countValue).length;
        group.countValuePct = group.countValueNum / group.data.length;
      }
    }
    return groups;
  }

  // Outputs object with useful info, like datatype, unique data
  // Can expect the group object to be validated
  descData(groups) { /* eslint class-methods-use-this: off */
    SignificanceTester.validateGroupsData(groups);

    let dataPrimitive = typeof groups[0].data[0];
    const uniqueValues = Array.from(new Set([].concat(...groups.map((group) => group.data))));
    if (dataPrimitive === 'string') uniqueValues.sort();
    if (dataPrimitive === 'number') uniqueValues.sort((a, b) => a - b);
    // Split number into ints or floats
    let dataType = null;
    if (dataPrimitive === 'number') {
      dataType = 'numerical';
      if (uniqueValues.every((e) => Number.isInteger(e))) {
        dataPrimitive = 'int';
      } else {
        dataPrimitive = 'float';
      }
    }

    if (dataPrimitive === 'string' || uniqueValues.length === 2) {
      dataType = 'categorical';
    }

    let dataDistribution = this.options.dataDistribution || 'unknown';
    delete this.options.dataDistribution;
    if (['unknown', 'guess'].includes(dataDistribution)) {
      // Guess data distribution based on data
      if (dataType === 'categorical') dataDistribution = 'categorical';
    }

    const groupsCount = groups.length;
    const sampleSize = groups.map((group) => group.data.length).reduce((a, b) => a + b);
    const hasNegativeValues = uniqueValues[0] < 0;
    return {
      dataPrimitive,
      dataType,
      dataDistribution,
      uniqueValues,
      hasNegativeValues,
      groupsCount,
      sampleSize
    };
  }

  // Expects groups array to be:
  // [ { name: 'control', data: [1, 0, ...] }
  //   { name: 'treatment', data: ... } ]
  static validateGroupsData(groups) {
    if (!Array.isArray(groups)) {
      throw new Error(`Expects array of group data objects. Instead received: ${typeof groups}`);
    }
    if (groups.length < 2) {
      throw new Error(`Expects at least 2 groups. Received ${groups.length} instead.`);
    }

    groups.forEach((group, index) => {
      if (typeof group !== 'object') {
        throw new Error('Expects each element of the array to be a group object. The object should look like: { data: [...], name: \'treatment group\'}');
      }
      if (!Object.prototype.hasOwnProperty.call(group, 'name')) {
        throw new Error(`Expects each group object to have a 'name' attribute. Group with index ${index} does not have a name.`);
      }
      if (!Object.prototype.hasOwnProperty.call(group, 'data') || !Array.isArray(group.data)) {
        throw new Error(`Expects group objects to have 'data' attribute with an array of data (primitives). Group '${group.name}' does not.`);
      }
      if (group.data.length < 1) {
        throw new Error(`Data array for group '${group.name}' is too short. Recevied data array of length: ${group.data.length}`);
      }
    });
    // Compare data types
    const dataTypes = new Set(
      [].concat(...groups.map((group) => group.data.map((el) => typeof el)))
    );
    if (dataTypes.size > 1) {
      throw new Error(`Expects consistent types of data in the group's data attributes. Instead received data types: ${Array.from(dataTypes).join(', ')}`);
    }
    if (!(dataTypes.has('string') || dataTypes.has('number'))) {
      throw new Error(`Expects data to be either string or number. Instead it was ${Array.from(dataTypes)}.`);
    }
  }
}

module.exports = SignificanceTester;
