# Simulator

This is a command line simulator tool to test if results from an A/B test are significant or not.

# Prerequisites

This tool requires the A/B test results in a comma separated CSV file with the columns `group` and `result`. Such a file could look like this:

```
group,result
3 options,red
3 options,blue
3 options,red
3 options,red
8 options,red
8 options,blue
8 options,blue
8 options,blue
```

# Build

1. Clone this repo and build with `npm run build`
1. Your binaries should be in the `build`-folder

# Usage

1. Navigate to your `build`-folder
1. Open a terminal and run the binary for your platform with argument `--file data-file.csv`
1. Optionally use `--help` to see which other arguments are supported.

# Comparison methods

The `--compare` argument lets you configure which method the tool should use to compare group A and B. Leave blank to let the tool guess based on data-type.

The tool supports the following options:

`--compare=mean`
Calculate the mean value of each group and compare the difference between them. Can be used when `result` column contains numbers/floats.

`--compare=median`
Same as mean-method, but uses the median value of each group. Can be used when `result` column contains numbers/integers.

`--compare=count-pct`
Compare how often a result occurs in each group, represented in a percentage. Can be used when `result` column contains categorical data/strings. (This method should be preferred instead of `count` as this accounts for uneven group distributions in source data.)

`--compare=count`
Same as count-pct, only this doesn't represent the result in a percentage. Only use if the groups are of equal size.


# Example output

```
./simulator-macos --file example-data/data.csv --sims=100000 --compare=count-pct

Group '3 navn' has 28 items.
Group '8 navn' has 28 items.
~79 % of group '3 navn' is 'Sunn' compared to ~54 % in group '8 navn', which is ~25 % less.

Ran 100000 simulations (of 100000).
┌───────────────────────┬───────────────────────┬──────────────────────┐
│        Diff results   │   Simulations count   │   % of simulations   │
│            - 53.6 %   │                   2   │              0.0 %   │
│            - 46.4 %   │                  27   │              0.0 %   │
│            - 39.3 %   │                 177   │              0.2 %   │
│            - 32.1 %   │                 919   │              0.9 %   │
│   (actual) - 25.0 %   │                3350   │              3.4 %   │
│            - 17.9 %   │                8464   │              8.5 %   │
│            - 10.7 %   │               15572   │             15.6 %   │
│             - 3.6 %   │               21391   │             21.4 %   │
│             + 3.6 %   │               21147   │             21.1 %   │
│            + 10.7 %   │               15761   │             15.8 %   │
│            + 17.9 %   │                8761   │              8.8 %   │
│            + 25.0 %   │                3314   │              3.3 %   │
│            + 32.1 %   │                 908   │              0.9 %   │
│            + 39.3 %   │                 184   │              0.2 %   │
│            + 46.4 %   │                  20   │              0.0 %   │
│            + 53.6 %   │                   3   │              0.0 %   │
└───────────────────────┴───────────────────────┴──────────────────────┘

   Simulations below actual:         95525      95.5 %   
   Simulations equal to actual:       3350       3.4 %   
   Simulations above actual:          1125       1.1 %   

```

*Explanation:*
In this case the hypothesis was that group `3 navn` would have more `Sunn` than group `8 navn`. Our results indicated this by a good margin (~25 %). After running this simulation we se that 95.5% of the simulations supports our result. The uncertainty is 4.5%. Given p=0.05 out results are significant.
