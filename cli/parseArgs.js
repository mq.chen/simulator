const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');

module.exports = () => {
  // CONFIG
  // args
  const argsDef = [
    {
      name: 'file',
      type: String,
      typeLabel: '{underline path to file}',
      description: 'The CSV data file. This tool will guess delimiter, escape character, etc. based on file contents.'
    },
    {
      name: 'dataDistribution',
      type: String,
      typeLabel: '{underline guess, normal, exponential}',
      description: 'Describe how the data is distributed. Helps pick best test method. https://www.analyticsvidhya.com/blog/2017/09/6-probability-distributions-data-science/'
    },
    {
      name: 'compare',
      type: String,
      typeLabel: '{underline mean, median, count, count-pct}',
      description: 'How to compare/diff groups? Options: mean, median or counts? Use \'guess\' to determine by data format.'
    },
    {
      name: 'simulations',
      type: Number,
      typeLabel: '{underline number}',
      description: 'How many simulations to run?'
    },
    {
      name: 'meanRoundDecimals',
      type: Number,
      typeLabel: '{underline number}',
      description: 'If compare method is mean, use this to specify how many decimals to round to. E.g.: 2 rounds 1.235 ≈ 1.24. and -2 rounds 123 ≈ 100.',
    },
    {
      name: 'countValue',
      type: String,
      typeLabel: '{underline string}',
      description: 'If compare method is count or count-pct use this to specify which item to count occurrences of. Leave blank to use first (sorted).'
    },
    {
      name: 'countPctRoundDecimals',
      type: Number,
      typeLabel: '{underline number}',
      description: 'If compare method is count-pct, use this to specify how many decimals to round to. E.g.: 2 rounds 1.235 % ≈ 1.24 %. and -2 rounds 123 % ≈ 100 %.',
    },
    {
      name: 'help',
      alias: 'h',
      type: Boolean,
      description: 'Prints this.',
      defaultValue: false
    }
  ];
  const args = commandLineArgs(argsDef);
  const usage = commandLineUsage([
    {
      header: 'SignificanceTester',
      content: 'Test significance using various methods.'
    },
    {
      header: 'Options',
      optionList: argsDef
    }
  ]);

  if (args.help || !args.file) {
    console.log(usage);
    console.log('Example args: --file=data.csv --simulations=100000 --compare=mean');
    return undefined;
  }

  return args;
};
