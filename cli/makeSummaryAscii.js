const Table = require('terminal-table');

module.exports = (simsSummary, actualDiff) => {
  const t = new Table({
    leftPadding: 3,
    rightPadding: 3,
    borderStyle: 2
  });
  t.push([
    'Diff results',
    'Simulations count',
    '% of simulations'
  ]);
  t.attrRange({ row: [0, 99999], column: [0, 99999] }, { align: 'right' });

  const diffs = Array.from(simsSummary.keys()).sort((a, b) => a - b);
  const totalDiffs = Array.from(simsSummary.values()).reduce((a, b) => a + b);

  let belowActualDiffCount = 0;
  let equalActualDiffCount = 0;
  let aboveActualDiffCount = 0;

  diffs.forEach((diff) => {
    let dispDiff = diff.toString();
    if (actualDiff.compare === 'count-pct') {
      dispDiff = `${(diff * 100).toFixed(Math.max(actualDiff.countPctRoundDecimals, 0))} %`;
    }

    // Add + in front of positive numbers
    if (diff > 0) {
      dispDiff = `+${dispDiff}`;
    }

    // Add space between
    dispDiff = dispDiff.replace(/^([-+]{1})/, '$1 ');

    // Annotate actual
    if (diff === actualDiff.diff) {
      dispDiff = `(actual) ${dispDiff}`;
    }

    const simsCount = simsSummary.get(diff);
    t.push([
      dispDiff,
      simsCount,
      `${((simsSummary.get(diff) / totalDiffs) * 100).toFixed(1)} %`
    ]);

    // Update counts
    if (diff < actualDiff.diff) belowActualDiffCount += simsCount;
    else if (diff === actualDiff.diff) equalActualDiffCount += simsCount;
    else aboveActualDiffCount += simsCount;
  });

  // Percentage of simulations compared to actual
  const pctBelowActual = (belowActualDiffCount / totalDiffs) * 100;
  const pctEqualActual = (equalActualDiffCount / totalDiffs) * 100;
  const pctAboveActual = (aboveActualDiffCount / totalDiffs) * 100;

  // Make summary table
  const s = new Table({
    leftPadding: 0,
    rightPadding: 3,
    borderStyle: 0,
    border: {
      sep: '',
      topLeft: '',
      topMid: '',
      top: '',
      topRight: '',
      midLeft: '',
      midMid: '',
      mid: '',
      midRight: '',
      botLeft: '',
      botMid: '',
      bot: '',
      botRight: ''
    }
  });
  s.push(['Simulations below actual:', `${aboveActualDiffCount}`, `${pctAboveActual.toFixed(1)} %`]);
  s.push(['Simulations equal to actual:', `${equalActualDiffCount}`, `${pctEqualActual.toFixed(1)} %`]);
  s.push(['Simulations above actual:', `${belowActualDiffCount}`, `${pctBelowActual.toFixed(1)} %`]);
  s.attrRange({ row: [0, 99999], column: [1, 99999] }, { align: 'right' });

  return `${t}\n${s}`;
};
