const fs = require('fs');
const CSVSniffer = require('csv-sniffer')();

function guessResultType(results) {
  // returns int, string, float

  // all ints?
  if (results.every((e) => parseInt(e, 10).toString() === e)) {
    return 'int';
  }

  // all floats?
  if (results.every((e) => parseFloat(e, 10).toString() === e)) {
    return 'float';
  }

  // all strings
  return 'string';
}

function castResults(results, type) {
  return results.map((el) => {
    switch (type) {
      case 'int': return parseInt(el, 10);
      case 'float': return parseFloat(el, 10);
      default: return el;
    }
  });
}

module.exports = (filename) => {
  // Parse CSV
  let original;
  let csv;
  try {
    const content = fs.readFileSync(filename, 'utf8');
    csv = (new CSVSniffer()).sniff(content, { hasHeader: true });
    // original = parseCsv(content);
    original = csv.records;
  } catch (e) {
    throw new Error(`Could not read file: ${filename}`);
  }

  // Find result and group columns
  const resultColumn = csv.labels.findIndex((el) => el.toLowerCase() === 'result');
  const groupColumn = csv.labels.findIndex((el) => el.toLowerCase() === 'group');
  if (resultColumn < 0 || groupColumn < 0) {
    throw new Error(`Could not find "Group" or "Result" column.\nColumns found: ${original[0].join(', ')}`);
  }
  let results = [];
  let groupNames = new Set();
  original.forEach((el) => {
    results.push(el[resultColumn]);
    groupNames.add(el[groupColumn]);
  });
  groupNames = Array.from(groupNames);

  // Guess result type
  const resultType = guessResultType(results);
  results = castResults(results, resultType);

  // Group results by group
  const groups = [];
  groupNames.forEach((group) => {
    let groupData = original.filter((row) => row[groupColumn] === group);
    groupData = groupData.map((row) => row[resultColumn]);
    groupData = castResults(groupData, resultType);
    groups.push({ name: group, data: groupData });
  });

  return groups;
};
