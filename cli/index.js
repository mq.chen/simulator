const chalk = require('chalk');
const parseArgs = require('./parseArgs');
const parseData = require('./parseData');
const SignificanceTester = require('../significanceTester');

// TODO: add support for --only flag
// TODO: add progress indicator for tests that support it
// TODO: add premise concept. Each test returns: premises [
//  { desc: 'Test assumes normal distribution', status: 'probably-pass' },
//  { desc: 'Test works best on larger samples', status: 'fail' },
//  { desc: 'Test assumes each sample is independent', status: 'unknown' }]
//  Status can be: pass, probably-pass, probably-fail, fail, unknown

try {
  // cli args
  const args = parseArgs() || process.exit(1);

  // parse data
  const data = parseData(args.file);

  const s = new SignificanceTester(data, args);

  // Input data
  console.log(chalk.inverse('\n Input data: '));
  console.log(`Sample size: ${s.dataDesc.sampleSize}. It is ${s.dataDesc.dataType} and consists of ${s.dataDesc.dataPrimitive}s.`);
  s.groups.forEach((group) => {
    let groupMeta = '';
    if (s.dataDesc.dataType === 'categorical') {
      groupMeta = `${group.countValueNum} counts of '${s.options.countValue}' (${(group.countValuePct * 100).toFixed(1)} %).`;
    }
    if (s.dataDesc.dataType === 'numerical') {
      if (s.options.compare === 'mean') groupMeta = `Mean value: ${group.mean}.`;
      if (s.options.compare === 'median') groupMeta = `Median value: ${group.median}.`;
    }
    console.log(`Group '${chalk.underline(group.name)}' has ${group.data.length} samples. ${groupMeta}`);
  });

  // Tests
  s.runTests();
  s.on('complete', (results) => {
    console.log('\n');
    const bestTest = results[0];

    results.reverse().forEach((test) => {
      if (test.testRelevance === 0) {
        console.log(chalk.grey(`${chalk.bold(test.testName)} was not relevant because:\n${test.testRelevanceReason}\n`));
        return;
      }
      console.log(chalk.inverse(`\n ${test.testName}: `));
      console.log(`${chalk.green('Test relevance:')} ${test.testRelevanceReason}`);
      console.log(`${chalk.yellow('Test limitations:')} ${test.testLimitations}`);

      // Calculation description
      if (test.testResult.calcDesc && test.testResult.calcDesc.type === 'text') {
        console.log(`\n${test.testResult.calcDesc.content}`);
      }

      // Summary
      let pctColor = chalk.red;
      if (test.testResult.oneTailedPValue < 0.05) pctColor = chalk.green;
      else if (test.testResult.oneTailedPValue < 0.1) pctColor = chalk.yellow;
      console.log(`\nTest shows you can be ${pctColor.call(null, ((1 - test.testResult.oneTailedPValue) * 100).toFixed(1))} % confident that your `
        + `directional hypothesis is valid. ${((1 - test.testResult.twoTailedPValue) * 100).toFixed(1)} % if your hypothesis is non-directional.`);

      // P-value
      console.log('\n');
      console.log(chalk.grey(`1-tailed p-value: ${test.testResult.oneTailedPValue.toFixed(5)}`));
      console.log(chalk.grey(`2-tailed p-value: ${test.testResult.twoTailedPValue.toFixed(5)}`));
    });

    // Most applicable test?
    if (results.length > 1) {
      console.log(`\n${chalk.inverse(' Best test: ')}`);
      const sameRelevanceAsBestTest = results.filter(
        (test) => test.testRelevance === bestTest.testRelevance
      );
      if (sameRelevanceAsBestTest.length > 1) {
        console.log(`The tests ${sameRelevanceAsBestTest.reduce((a, b) => `${chalk.bold.underline(a.testName)}, ${chalk.bold.underline(b.testName)}`)} `
          + 'are the most relevant tests.');
        console.log('Consider their limitations when selecting your test result.');
        console.log(chalk.grey('If you are uncertain, select the test with the lowest confidence.'));
      } else {
        console.log(`The ${chalk.bold.underline(bestTest.testName)} is the most relevant test, considering the input data.`);
      }
    }
  });
} catch (e) {
  console.error(e);
}
